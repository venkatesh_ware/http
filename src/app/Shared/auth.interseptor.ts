import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { LoderHandlerService } from "./loder-handler.service";


@Injectable({
    providedIn:"root"
})
export class AuthInterceptorService implements HttpInterceptor{
 
    constructor(private loderHandlerService:LoderHandlerService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("API call are going through  interceptor");
        console.log("service in interceptor", this.loderHandlerService);
     
        this.loderHandlerService.isLoader.next(true)
       
        let modifiedReq=req.clone({
            headers:req.headers.append('authorization','Berear token key') // modified req>> used send clone method to send object
        })

        return next.handle(modifiedReq)
        // return next.handle(req)
    }



 
}