import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { iForm } from './form';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class PostapiService {

  baseURL: string = "https://basichttpcall-default-rtdb.firebaseio.com/posts.json"
  baseNewURL: string = "https://basichttpcall-default-rtdb.firebaseio.com"

  constructor(private http: HttpClient) { }

  public fetchPost() {
    let httpHeaders=new HttpHeaders({
      "content Type":"Application/json",
      "authorization":"berar Token key"
    })
    return this.http.get<{[key:string]:iForm}>(this.baseURL ).pipe(
      map((res) => {
        const Arr: iForm[] = []
        if (res) {
          Object.entries(res).forEach(([key, value]) => {
            Arr.push({ ...value, id: key })
          }) 

        } return Arr;
      })
    )
  }


  public creatpost(post: iForm) {
    return this.http.post(this.baseURL, post)
  }


  public deletPost(id: string) {
    let delteURL = ` ${this.baseNewURL}/posts/${id}/.json`
    return this.http.delete(delteURL)
  }

  public update(id:string,body:iForm){
    let patchURL=`${this.baseNewURL}/posts/${id}/.json`
    return this.http.patch(patchURL,body)
  }


}
