import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { iForm } from '../Shared/form';
import { map } from "rxjs/operators"
import { PostapiService } from '../Shared/postapi.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoderHandlerService } from '../Shared/loder-handler.service';

@Component({
  selector: 'app-httpform',
  templateUrl: './httpform.component.html',
  styleUrls: ['./httpform.component.scss']
})
export class HttpformComponent implements OnInit {
  public creatPostForm!: FormGroup
  public loadedPost: iForm[] = []
  public isUpdated: boolean = false;
  public errorMsg!: string
  public loader: boolean = false;
  // baseURL: string = "https://basichttpcall-default-rtdb.firebaseio.com/posts.json"

  constructor(private http: HttpClient,
    private postapiService: PostapiService,
    private snackBar: MatSnackBar,
    private loderHandlerService: LoderHandlerService
  ) { }

  ngOnInit(): void {
    this.creationform()

    this.loderHandlerService.isLoader.subscribe((data) => {
      this.loader = data;
      console.log("datat", data);//true
    })

    this.fetchDatabase()  // get API call
  }
  // Form
  creationform() {
    this.creatPostForm = new FormGroup({
      title: new FormControl(null),
      postContent: new FormControl(null)
    })
  }

// get API call
  fetchDatabase() {
    // this.loader=true 
    this.postapiService.fetchPost().subscribe((data) => {
      if (data) {
        console.log("data",data);
        
        this.loadedPost = data
        this.loader = false
      }
    }
      , responceError => {
        this.loader = false
        // console.log("responceError", responceError);
        this.errorMsg = responceError.error.error
        this.snackBar.open(responceError.error.error, "close", {
          duration: 7000,
          verticalPosition: "top",
          horizontalPosition: "end"
        })

      })
  }
  // call back function

  onCreatSubmit() {
    // this.loader = true
    // console.log("creatPostForm", this.creatPostForm.value);
    let formObj = this.creatPostForm.value

    this.postapiService.creatpost(formObj).subscribe((data) => {
      this.loader = false
      console.log(data);
      let obj: iForm = {
        title: formObj.title,
        postContent: formObj.postContent,
        id: Object.entries(data)[0][1]
      }
      this.loadedPost.push(obj)

    })

    this.creatPostForm.reset()
  }



  /****On Delet callback Function */
  onDeletePost(post: iForm) {
    // this.loader = true
    console.log("id", post.id);
    this.loadedPost = this.loadedPost.filter((p) => {
      return p.id != post.id
    })

    if (post.id) {
      this.postapiService.deletPost(post.id)
        .subscribe((res) => {
          console.log("delt call", res);

        })
    }
    setTimeout(() => {
      this.loader = false

    }, 5000);
  }


  //*****callback function for Edit

  onEditPost(post: iForm) {

    console.log("id", post.id);
    if (post.id) {
      localStorage.setItem("Id", post.id)
      this.creatPostForm.patchValue({
        title: post.title,
        postContent: post.postContent
      })
    }
  }

  //**********On update button  */

  onUpdate() {
    // console.log(this.creatPostForm.value);
    let id: any = localStorage.getItem("Id")
    // console.log(id);

    let formObj = this.creatPostForm.value
    this.postapiService.update(id, formObj)
      .subscribe((res) => {
        // console.log("res",res);

        this.loadedPost.map((oldObj) => {
          console.log("oldObj", oldObj);

          if (oldObj.id == id) {
            oldObj.title = formObj.title;
            oldObj.postContent = formObj.postContent;
          }
        })

      })

    this.isUpdated = false;
    this.creatPostForm.reset();

  }




}
