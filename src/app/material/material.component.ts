import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit {
  ngOnInit(): void {

  }


  durationInSeconds = 5;

  constructor(private _snackBar: MatSnackBar) {}

  openSnackBar() {
    this._snackBar.open("this is snakbar", "hello",{
      duration: this.durationInSeconds * 1000,
      verticalPosition:"top",
      horizontalPosition:"left"
    });
  }
}

 